 #ifndef NRS_INTERFACE_H
#define NRS_INTERFACE_H

#include <windows.h>
#include <fstream>
#include <string>
#include <iostream>
#include "console_colors.h"

class NRS_interface {
	
	HANDLE hOut;
	HANDLE hIn;
	COORD cPos, cBuf;
	std::ifstream ifLogo;
	std::ifstream ifNRS;
	std::ifstream ifMain;
	SMALL_RECT srBuf;

public:
	NRS_interface();
	~NRS_interface();

	void SetScreenValues();
	void DisplayLogo();
	void DisplayInterface();
	void DisplayMenu();
};

#endif