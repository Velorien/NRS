#include "NRS_player.h"

int NRS_player::TestDest(char cDest){
	switch(cDest){ //0 means inability to cross given terrain tile
	case 'r':
		return 1;
	case 'v':
		return 0;
	case 'w':
		return 20;
	case 's':
		return 10;
	case 'm':
		return 3;
	case 'd':
		return 0;
	case 'c':
		return 4;
	case 'f':
		return 4;
	case 'l':
		return 2;
	case 'g':
		return 0;
	case 'q':
		return 22;
	default:
		return 0;
	}
	/*
	r - road
	v - void
	w - water
	s - sand
	m - moors
	q - swamp
	d - rock
	c - cave
	f - forest
	l - meadow
	g - deep water
	*/
}

//metoda do poruszania si� wed�ug inputu z klawiatury/AI
void NRS_player::Move(DIRECTION dDir, char** cMap){


	char cDest;
	
	switch(dDir){
	case UP:
		if(cPlayerPos.Y == 1)
			break;
		else{
			cDest = cMap[cPlayerPos.Y-2][cPlayerPos.X-1];
			dPlayerDirection = UP;
			if(TestDest(cDest)){
				cPlayerPos.Y--;
				MoveTimer = TestDest(cDest) + nSpeed;
			}
		}
		break;
	case RIGHT:
		if(cPlayerPos.X == 79)
			break;
		else{
			cDest = cMap[cPlayerPos.Y-1][cPlayerPos.X];
			dPlayerDirection = RIGHT;
			if(TestDest(cDest)){
				cPlayerPos.X++;
				MoveTimer = TestDest(cDest) + nSpeed;
			}
		}
		break;
	case DOWN:
		if(cPlayerPos.Y == 38)
			break;
		else{
			cDest = cMap[cPlayerPos.Y][cPlayerPos.X-1];
			dPlayerDirection = DOWN;
			if(TestDest(cDest)){
				cPlayerPos.Y++;
				MoveTimer = TestDest(cDest) + nSpeed;
			}
		}
		break;
	case LEFT:
		if(cPlayerPos.X == 1)
			break;
		else{
			cDest = cMap[cPlayerPos.Y-1][cPlayerPos.X-2];
			dPlayerDirection = LEFT;
			if(TestDest(cDest)){
				cPlayerPos.X--;
				MoveTimer = TestDest(cDest) + nSpeed;
			}
		}
		break;
	}


}

//przywali� - komu i za ile DMG?
void NRS_player::GetDamage(int nDamage, bool player, NRS_renderer* NRSRenderer){
	nCurrHealth-=nDamage;
	if(player)
		NRSRenderer->RenderPlayerHealthbar(nHealth, nCurrHealth);
}


///////////////////////////////////////////////////////////////////////////////////
////////////////SMOKI/////////////////SMOKI//////////////SMOKI/////////////////////
///////////////////////////////////////////////////////////////////////////////////


NRS_dragon::NRS_dragon(int a, int b){
	nHealth = 100;
	nDamage = 20;
	nSpeed = 2;
	nReload = 50;
	nRegen = 10;

	eKind = CREATURE;
	cSymbol = (char)209;
	dPlayerDirection = UP;
	nCurrHealth = 100;
	RegenTimer = MoveTimer = ShootTimer = 0;

	cPlayerPos.X = a;
	cPlayerPos.Y = b;
}

DragonBreath::DragonBreath(int a, int b, int c){
	eKind = DRAGONBREATH;
	FireDuration = 3;
	cPlayerPos.X = a;
	cPlayerPos.Y = b;
	dPlayerDirection = static_cast<NRS_player::DIRECTION>(c);
	switch(c){
	case 0:
		cPlayerPos.Y--;
		break;
	case 1:
		cPlayerPos.X++;
		break;
	case 2:
		cPlayerPos.Y++;
		break;
	case 3:
		cPlayerPos.X--;
		break;
	}
}

//incjalizacja zioni�cia
void NRS_dragon::Shoot(NRS_player** EntityTab, int EntQuan){
	ShootTimer = nReload;
	for(int i = 0; i < EntQuan; i++){
		if(EntityTab[i] != NULL)
			continue;
		else{
			EntityTab[i] = new DragonBreath(cPlayerPos.X, cPlayerPos.Y, dPlayerDirection);
			break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
//////////////NAZI�CI I RUSCY//////////////////////NAZI�CI I RUSCY///////////////
/////////////////////////////////////////////////////////////////////////////////


NRS_nazi::NRS_nazi(int a, int b){
	nHealth = 15;
	nDamage = 4;
	nSpeed = 1;
	nReload = 20;
	nRegen = 25;

	eKind = CREATURE;
	cSymbol = 'N';
	dPlayerDirection = UP;
	nCurrHealth = 15;
	RegenTimer = MoveTimer = ShootTimer = 0;

	cPlayerPos.X = a;
	cPlayerPos.Y = b;
}

Bullet::Bullet(int a, int b, int c){
	eKind = BULLET;
	FireDuration = 1;
	cPlayerPos.X = a;
	cPlayerPos.Y = b;
	cSymbol = char(250);
	dPlayerDirection = static_cast<NRS_player::DIRECTION>(c);
	switch(c){
	case 0:
		cPlayerPos.Y--;
		break;
	case 1:
		cPlayerPos.X++;
		break;
	case 2:
		cPlayerPos.Y++;
		break;
	case 3:
		cPlayerPos.X--;
		break;
	}
}

NRS_russki::NRS_russki(int a, int b){
	nHealth = 10;
	nDamage = 4;
	nSpeed = 0;
	nReload = 15;
	nRegen = 25;

	eKind = CREATURE;
	cSymbol = 'R';
	dPlayerDirection = UP;
	nCurrHealth = 10;
	RegenTimer = MoveTimer = ShootTimer = 0;

	cPlayerPos.X = a;
	cPlayerPos.Y = b;
}

//Ruski strzela
void NRS_russki::Shoot(NRS_player** EntityTab, int EntQuan){
	ShootTimer = nReload;
	for(int i = 0; i < EntQuan; i++){
		if(EntityTab[i] != NULL)
			continue;
		else{
			EntityTab[i] = new Bullet(cPlayerPos.X, cPlayerPos.Y, dPlayerDirection);
			break;
		}
	}
}

//Nazista strzela
void NRS_nazi::Shoot(NRS_player** EntityTab, int EntQuan){
	ShootTimer = nReload;
	for(int i = 0; i < EntQuan; i++){
		if(EntityTab[i] != NULL)
			continue;
		else{
			EntityTab[i] = new Bullet(cPlayerPos.X, cPlayerPos.Y, dPlayerDirection);
			break;
		}
	}
}