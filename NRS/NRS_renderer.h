#ifndef NRS_RENDERER_H
#define NRS_RENDERER_H

#include <iostream>
#include <fstream>
#include <Windows.h>
#include <string>
#include "console_colors.h"

using namespace std;

class NRS_renderer{

	ifstream ifCommonMap;
	HANDLE hOut;
	COORD cPos;

public:
	char** cMapTab;
	NRS_renderer(); /* 1-79, 1-38 */
	~NRS_renderer();
	void RenderMap();
	void WriteMapPoint(char);
	void RestoreMapPoint(COORD);
	void RenderPlayerHealthbar(int, int);
	void DrawDirectionMark(int);
	void RenderEntity(COORD, char);

};


#endif