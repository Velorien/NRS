#include "NRS_interface.h"
#include "NRS_renderer.h"
#include "NRS_game.h"
#include <conio.h>

using namespace std;

int main(){
	NRS_interface NRSConsole;
	NRS_renderer NRSRenderer;
	NRS_game NRSGame;
	
	NRSConsole.SetScreenValues();
	NRSConsole.DisplayLogo();
	NRSConsole.DisplayMenu();
	
	NRSGame.MainGameLoop(&NRSConsole, &NRSRenderer);

	getch();
	return 0;
}

