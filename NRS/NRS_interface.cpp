#include "NRS_interface.h"

#define CONSOLE_HEIGHT 40
#define CONSOLE_WIDTH 100

using namespace std;

NRS_interface::NRS_interface() //le konstruktor
	:
	hOut(GetStdHandle(STD_OUTPUT_HANDLE)),
	hIn(GetStdHandle(STD_INPUT_HANDLE)),
	ifLogo("cg.txt"),
	ifNRS("nrs.txt"),
	ifMain("main.txt")
{
	srBuf.Bottom = CONSOLE_HEIGHT;
	srBuf.Left = 0;
	srBuf.Right = CONSOLE_WIDTH;
	srBuf.Top = 0;
	cPos.X = 0;
	cPos.Y = 0;
	cBuf.X = 101;
	cBuf.Y = 41;
}

NRS_interface::~NRS_interface(){ //le destruktor
	ifLogo.close();
	ifNRS.close();
	ifMain.close();
}

void NRS_interface::SetScreenValues(){ //ustawia rozmiar konsoli
	CONSOLE_CURSOR_INFO ConCurInf;
	ConCurInf.bVisible = false;
	ConCurInf.dwSize = 1;
	SetConsoleCursorInfo(hOut, &ConCurInf);
	SetConsoleTitle(TEXT("Nazisci vs Ruscy vs Smoki"));
	SetConsoleScreenBufferSize(hOut, cBuf);
	SetConsoleWindowInfo(hOut, TRUE, &srBuf);
}

void NRS_interface::DisplayLogo(){ //wyswietla logo CG
	std::string sLine;

	cPos.X = 0;
	cPos.Y = 17;
	SetConsoleCursorPosition(hOut, cPos);
	SetConsoleTextAttribute(hOut, FBI);
	if(ifLogo.is_open()){
		while(ifLogo.good()){
			getline(ifLogo, sLine);
			cout << sLine << endl;
		}
	}
	
	Sleep(2000);
	system("color 10");
	Sleep(500);
	system("CLS");

	SetConsoleTextAttribute(hOut, FRI);
	cPos.X = 0;
	cPos.Y = 5;
	system("CLS");
	SetConsoleCursorPosition(hOut, cPos);

	if(ifNRS.is_open()){
		while(ifNRS.good()){
			getline(ifNRS, sLine);
			cout << sLine << endl;
		}
	}

	Sleep(2000);
	system("color c0");
	Sleep(500);
	system("CLS");
	system("color 07");
}

void NRS_interface::DisplayInterface(){ //wyswietla interface gracza, W 3.14ZDU KODU KTORY MALO ROBI
	char cDraw;
	SetConsoleTextAttribute(hOut, FWI);

	for(int i = 1; i < CONSOLE_HEIGHT; i++){
		cDraw = (char)186;
		cPos.Y = i;
		cPos.X = 0;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
		cPos.X = 100;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}

	for(int i = 1; i <= CONSOLE_WIDTH; i++){
		cDraw = (char)205;
		cPos.X = i;
		cPos.Y = 0;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
		cPos.Y = 39;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}

	cPos.X = cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)201;
	cout << cDraw;

	cPos.X = 0;
	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)200;
	cout << cDraw;

	cPos.X = 100;
	cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)187;
	cout << cDraw;

	cPos.X = 100;
	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)188;
	cout << cDraw;
	
	/* pionowa kreska oddzielajaca mape od menu bocznego */
	cPos.X = 80;
	cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)203;
	cout << cDraw;

	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)202;
	cout << cDraw;
	
	cDraw = (char)186;

	for(int i = 1; i < 39; i++){
		cPos.Y = i;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}

	cPos.X = 82;
	cPos.Y = 5;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "HEALTH";

	cPos.X = 89;
	cPos.Y = 8;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)201;
	cout << cDraw;
	cDraw = (char)202;
	cout << cDraw;
	cDraw = (char)187;
	cout << cDraw;
	cPos.X = 89;
	cPos.Y = 9;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)185;
	cout << cDraw;
	cDraw = (char)233;
	cout << cDraw;
	cDraw = (char)204;
	cout << cDraw;
	cPos.X = 89;
	cPos.Y = 10;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)200;
	cout << cDraw;
	cDraw = (char)203;
	cout << cDraw;
	cDraw = (char)188;
	cout << cDraw;
}

void NRS_interface::DisplayMenu(){ //wyswietla menu glowne

	if(ifMain.is_open())
		while(ifMain.good()){
			string sLine;

			getline(ifMain, sLine);
			cout << sLine << endl;
		}

	SetConsoleTextAttribute(hOut, BW);

	cPos.X = 11;
	cPos.Y = 19;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 11;
	cPos.Y = 20;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 11;
	cPos.Y = 21;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 41;
	cPos.Y = 14;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 41;
	cPos.Y = 15;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 41;
	cPos.Y = 16;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 76;
	cPos.Y = 13;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 76;
	cPos.Y = 14;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 76;
	cPos.Y = 15;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";
	
	SetConsoleTextAttribute(hOut, BWI);

	cPos.X = 10; //rysuje przycisk NAZISCI
	cPos.Y = 18;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.Y = 19;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "  NAZISCI  ";

	cPos.Y = 20;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 40; //rysuje przycisk RUSCY
	cPos.Y = 13;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.Y = 14;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "   RUSCY   ";

	cPos.Y = 15;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 75; //rysuje przycisk SMOKI
	cPos.Y = 12;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.Y = 13;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "   SMOKI   ";

	cPos.Y = 14;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "           ";

	cPos.X = 80;
	cPos.Y = 39;
	SetConsoleTextAttribute(hOut, FBI);
	SetConsoleCursorPosition(hOut, cPos);
	cout << "Clockwork Games 2011";
}
